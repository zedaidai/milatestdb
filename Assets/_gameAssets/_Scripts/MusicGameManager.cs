﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicGameManager : Singleton<MusicGameManager>
{
    public DoughAnims DoughYou;
    public DoughAnims DoughCpu;
    public Generator GeneratorYou;
    public Generator GeneratorCpu;
    public TextAsset jsonDataText;
    JSONObject jsonData;

    AudioSource music;
    float startTime;
    int currentIndex;
    void Start()
    {
        music = this.GetComponent<AudioSource>();
        music.Play();
        currentIndex = 0;
        startTime = Time.time;   
        jsonData = new JSONObject(jsonDataText.text)["music1"];
    }

    void Update()
    {
        if(currentIndex < jsonData.list.Count &&  Time.time - startTime > jsonData.list[currentIndex]["t"].f ){
            GeneratorCpu.GenerateBall((int)jsonData.list[currentIndex]["b"].f, 0);
            GeneratorYou.GenerateBall((int)jsonData.list[currentIndex]["b"].f, 4);
            currentIndex ++;
        }
    }



    //* input events */
    public void PushBt1(){
        DoughYou.PlayJump1();
    }

    public void PushBt2(){
        DoughYou.PlayJump2();
    }
}
