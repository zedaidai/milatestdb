﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoughAnims : MonoBehaviour
{
    public void PlayJump1(){
        StopAllCoroutines();
        StartCoroutine(c_PlayJump1());
    }

    IEnumerator c_PlayJump1(){
        SelectSpriteFrame("jump1");
        yield return new WaitForSeconds(0.3f);
        SelectSpriteFrame("idle");
    }

    public void PlayJump2(){
        StopAllCoroutines();
        StartCoroutine(c_PlayJump2());
    }

    IEnumerator c_PlayJump2(){
        SelectSpriteFrame("jump2");
        yield return new WaitForSeconds(0.3f);
        SelectSpriteFrame("idle");
    }

    void SelectSpriteFrame(string frame){
        Transform child;
        for(int i = 0; i < transform.childCount; i++){
            child = transform.GetChild(i);
            if(child.name == frame){
                child.gameObject.SetActive(true);
            } else {
                child.gameObject.SetActive(false);
            }
        }
    }
}
