﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Generator : MonoBehaviour
{
    public GameObject BallPrefab;

   public void GenerateBall(int ballType, float delay){
       StartCoroutine(C_SpawnBall(delay));
   }

   IEnumerator C_SpawnBall(float delay){
       yield return new WaitForSeconds(delay);
       GameObject ball = Instantiate(BallPrefab, new Vector3(), Quaternion.Euler(0, 0, 0), transform);
       ball.transform.localPosition = new Vector3(0, 0, 0);
   }
}
